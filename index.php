<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>S2 - A1</title>
   <style>
      h2{
         text-align: center;
      }
      div{
         padding: 10px;
         border: 1px black solid;
         margin: 20px auto;
         width: fit-content;
      }
   </style>
</head>
<body>
   <div>
      <h2>Divisible by 5!</h2>
      <p><?php divBy5() ?></p>
   </div>

   <div>
      <h2>Array Manipulation</h2>
      <?php array_push($students, "John Smith") ?>
      <p><?php print_r($students) ?></p>
      <p><?= count($students) ?></p>
      <?php array_push($students, "Jane Smith") ?>
      <p><?php print_r($students) ?></p>
      <p><?= count($students) ?></p>
      <?php array_shift($students) ?>
      <p><?php print_r($students) ?></p>
      <p><?= count($students) ?></p>
   </div>
</body>
</html>